const selectAllButton = document.getElementById('selectAllBtn');
const inputField = document.getElementById('input');
const todoListUlElement = document.getElementById("list");
const actionPanel2 = document.getElementById('actionPanel2');


let todoList = [];


selectAllButton.addEventListener('click', () => {
    todoList.map(item => {
        item.isChecked = true
    });
    updateToDoItems()
});

inputField.addEventListener('keydown', event => {
    if ((event.key === "Enter" || event.keyCode === 13) && (inputField.value !== '')) {
        console.log(inputField.value);
        todoList.unshift(
            {value: inputField.value, isDone: false, isChecked: false}
        );
        inputField.value = '';
        updateToDoItems()
    }
});

const updateToDoItems = () => {
    todoListUlElement.innerHTML = '';

    todoList.some(item => item.isChecked) ? actionPanel2.style.display = "flex" : actionPanel2.style.display = "none";

    todoList.forEach((item, index) => {
        const liElement = document.createElement('li');
        liElement.className = "list-group-item";

        todoListUlElement.append(liElement);

        const divElement = document.createElement('div');
        divElement.className = "form-group form-check";
        liElement.append(divElement);

        const checkBoxElement = document.createElement('input');
        checkBoxElement.type = "checkbox";
        checkBoxElement.classname = "form-check-input";
        checkBoxElement.id = `exampleCheck${index}`;
        checkBoxElement.checked = item.isChecked;
        divElement.append(checkBoxElement);

        const labelElement = document.createElement('label');
        labelElement.className = item.isDone ? "form-check-label todoDone" : "form-check-label";
        labelElement.setAttribute('for', `exampleCheck${index}`);
        labelElement.innerText = item.value;
        divElement.append(labelElement);

        const doneButtonElement = document.createElement('button');
        doneButtonElement.className = "btn btn-outline-primary";
        doneButtonElement.type = "button";
        doneButtonElement.innerText = !item.isDone ? "done" : "undone";
        doneButtonElement.style.cssFloat = "right";
        divElement.append(doneButtonElement);

        const deleteButtonElement = document.createElement('button');
        deleteButtonElement.className = "btn btn-outline-danger";
        deleteButtonElement.type = "button";
        deleteButtonElement.innerText = "remove";
        deleteButtonElement.style.cssFloat = "right";
        divElement.append(deleteButtonElement);

        doneButtonElement.addEventListener('click', () => {
            labelElement.classList.toggle("todoDone");
            item.isDone = !item.isDone;
            updateToDoItems();
        });

        checkBoxElement.addEventListener('change', () => {
            item.isChecked = checkBoxElement.checked;
            updateToDoItems();
        });

        deleteButtonElement.addEventListener('click', () => {
            todoList.splice(index, 1);
            updateToDoItems();
        })
    })
};


document.getElementById('doneAction').addEventListener('click', () => {
    todoList.filter(item => item.isChecked).forEach((item) => {
        item.isDone = true;
    });
    updateToDoItems();
});

document.getElementById('restoreAction').addEventListener('click', () => {
    todoList.filter(item => item.isChecked).forEach((item) => {
        item.isDone = false;
    });
    updateToDoItems();
});

document.getElementById('removeAction').addEventListener('click', () => {
    todoList = todoList.filter(item => !item.isChecked);
    updateToDoItems();
});